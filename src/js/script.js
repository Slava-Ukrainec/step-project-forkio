$(document).ready(function(){
    $('.carousel').slick(
        { 
        infinite: true,
        speed: 500,
        cssEase: 'linear',
        nextArrow:`<div class="slick-next"><i class="fas fa-greater-than"></i></div>`,
        prevArrow:`<div class="slick-prev"><i class="fas fa-less-than"></i></div>`,
        mobileFirst:true,
        lazyLoad: 'ondemand'
    }
    );
  }
  );
 let media = () => {if (window.innerWidth > 480) {
    let link = document.createElement('link');
    link.rel = 'stylesheet';
    link.href = 'css/media.css';
    document.getElementsByTagName('head')[0].appendChild(link);
}
 }

 let changeText =()=>{window.innerWidth>767 ?(
    document.getElementById("callbackHeadline").innerText="Order callback",
    document.getElementById("buttonSubcsribe").innerText="call me!"
  ) :(
    document.getElementById("callbackHeadline").innerText="Subscribe To Stay In Touch",
    document.getElementById("buttonSubcsribe").innerText="subscribe")
    media()
}
changeText();
window.addEventListener(`resize`,changeText)

