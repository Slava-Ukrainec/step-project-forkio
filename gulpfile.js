const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync').create();
const concat = require('gulp-concat');
const terser = require('gulp-terser');
const rename = require('gulp-rename');
const autoprefixer = require('gulp-autoprefixer');
const pump = require('pump');
const cleanCSS = require('gulp-clean-css');
const clean = require('gulp-clean');
const imagemin = require('gulp-imagemin');
sass.compiler = require('node-sass');

const cleanDist = () => {
    return gulp.src('./dist', {
        read: false,
        allowEmpty: true
    })
        .pipe(clean());
    
};

const copyHTML = (cb) => {
    gulp.src('*.html').pipe(gulp.dest('./dist/'));
    cb();
};

const compileSCSS = (cb) => {
    pump([
        gulp.src('src/scss/**/*.scss'),
        sass({outputStyle: 'compressed'}).on('error', sass.logError),
        autoprefixer({
            browsers: ['last 10 versions'],
            cascade: false
        }),
        cleanCSS({compatibility: 'ie8'}),
        browserSync.reload({
            stream: true
        }),
        gulp.dest('./dist/css/')
    ], cb)
};

const copyImages = () => {
   return gulp.src('src/img/*')
        .pipe(imagemin())
        .pipe(gulp.dest('./dist/img/'));
    
};

const copyJS = (cb) => {
    pump([
        gulp.src(['src/js/*.js','!src/js/slick.min.js']),
        concat('all.js'),
        terser(), // uglify analogue but with support of ES6
        rename('script.min.js'),
        gulp.dest('./dist/js/'), 
    ],gulp.src('src/js/slick.min.js').pipe(gulp.dest('./dist/js/')), cb())
};

const brSync = (cb) => {
    browserSync.init({
        server: {
            baseDir: "./dist/"
        },
        port: 8080,
        open: true,
        notify: false,
        browser: 'Firefox'
        // browser: 'Chrome'
    });
    gulp.watch('src/scss/*.scss').on('change', compileSCSS);
    gulp.watch('*.html').on('change', gulp.series(copyHTML, browserSync.reload));
    gulp.watch('src/js/*.js').on('change', gulp.series(copyJS, browserSync.reload));
    cb();
};

gulp.task('build', gulp.series(cleanDist, gulp.parallel(copyImages, copyHTML, compileSCSS, copyJS)));
gulp.task('dev', brSync);