let header = document.getElementById("header");
let burger = document.getElementById("burgerCheckbox");
let menuItem = document.querySelectorAll(".header__menu-item");
let menu = document.querySelector('.header__menu');


// REMOVING DELAY IN HOVER ANIMATION WHEN MENU IS OPEN
burger.addEventListener("click", function (event) {
    if (burger.checked === true) {
        setTimeout(function () {
            menuItem.forEach((item) => {
                item.style.transition = '0s';
            })
        }, 550)//
    } else {
            menuItem.forEach((item,i) => {
                item.style.transition = '0.5s';
                item.style.transitionDelay = `${(++i-1)*0.05}s`
            })
            }

});

// CLOSING MENU AFTER CHOOSING ITEM
menuItem.forEach(item => {
    item.addEventListener('click', function (event) {
        burger.checked = false;
    })
})

// CLOSING MENU IF WINDOW IS BROADER THAN 767
window.addEventListener('resize', function (event) {
    if (window.innerWidth > 767 && burger.checked === true) {
        burger.checked = false
    }
});

// SCROLLS
window.addEventListener("scroll", function (event) {
    if (burger.checked === false) {
        header.classList[
            pageYOffset >= 120 ? 'add' : 'remove'
            ]('header_scrolled')
    }
})

